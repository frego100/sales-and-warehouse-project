export interface BaseInterface {
    id?: number;
    createdat?: Date;
    updatedat?: Date;
    estado?: boolean;
}
