import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { EMPLOYE_PREFIX } from "../../shared/prefix/api.prefix";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { EmployeInterface } from "../../interfaces";

@Injectable({
  providedIn: "root",
})
export class EmployeService {
  url = `${environment.api}${EMPLOYE_PREFIX}employe`;

  constructor(
    private http: HttpClient) {
      console.log(this.url);
    }

  public getRegistros(): Observable<EmployeInterface[]> {
    return this.http.get<EmployeInterface[]>(`${this.url}`);
  }

  public getRegistrosByQuery(params: HttpParams): Observable<EmployeInterface[]> {
    return this.http.get<EmployeInterface[]>(`${this.url}`, {params});
  }

  public getRegistroById(id: number): Observable<EmployeInterface> {
    return this.http.get<EmployeInterface>(`${this.url}/${id}`);
  }

  public getRegistroByIdByQuery(id: number, params: HttpParams): Observable<EmployeInterface> {
    return this.http.get<EmployeInterface>(`${this.url}/${id}`, {params});
  }

  public postRegistro(registro: EmployeInterface): Observable<EmployeInterface> {
    return this.http.post<EmployeInterface>(`${this.url}`, registro);
  }

  public patchRegistro(registro: EmployeInterface): Observable<EmployeInterface> {
    return this.http.patch<EmployeInterface>(`${this.url}/${registro.id}`, registro);
  }

  public postRegistros(registros: EmployeInterface[]): Observable<EmployeInterface[]> {
    return this.http.post<EmployeInterface[]>(`${this.url}/bulk`, {bulk: registros});
  }

  public putRegistro(registro: EmployeInterface): Observable<EmployeInterface> {
    return this.http.put<EmployeInterface>(`${this.url}/${registro.id}`, registro);
  }

  public deleteRegistro(id: number): Observable<EmployeInterface> {
    return this.http.delete<EmployeInterface>(`${this.url}/${id}`);
  }
}
