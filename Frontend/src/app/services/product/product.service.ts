import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { PRODUCT_PREFIX } from '../../shared/prefix/api.prefix';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductsInterface } from '../../interfaces';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url = `${environment.api}${PRODUCT_PREFIX}`;
  
  constructor(
    private http: HttpClient) {
      console.log(this.url);
    }

  public getRegistros(): Observable<ProductsInterface[]> {
    return this.http.get<ProductsInterface[]>(`${this.url}`);
  }

  public getRegistrosByQuery(params: HttpParams): Observable<ProductsInterface[]> {
    return this.http.get<ProductsInterface[]>(`${this.url}`, {params});
  }

  public getRegistroById(id: number): Observable<ProductsInterface> {
    return this.http.get<ProductsInterface>(`${this.url}/${id}`);
  }

  public getRegistroByIdByQuery(id: number, params: HttpParams): Observable<ProductsInterface> {
    return this.http.get<ProductsInterface>(`${this.url}/${id}`, {params});
  }

  public postRegistro(registro: ProductsInterface): Observable<ProductsInterface> {
    return this.http.post<ProductsInterface>(`${this.url}`, registro);
  }

  public patchRegistro(registro: ProductsInterface): Observable<ProductsInterface> {
    return this.http.patch<ProductsInterface>(`${this.url}/${registro.id}`, registro);
  }

  public postRegistros(registros: ProductsInterface[]): Observable<ProductsInterface[]> {
    return this.http.post<ProductsInterface[]>(`${this.url}/bulk`, {bulk: registros});
  }

  public putRegistro(registro: ProductsInterface): Observable<ProductsInterface> {
    return this.http.put<ProductsInterface>(`${this.url}/${registro.id}`, registro);
  }

  public deleteRegistro(id: number): Observable<ProductsInterface> {
    return this.http.delete<ProductsInterface>(`${this.url}/${id}`);
  }
}
