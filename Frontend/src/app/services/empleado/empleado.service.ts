import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { EMPLEADO_PREFIX } from '../../shared/prefix/api.prefix';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EmpleadoInterface } from '../../interfaces';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {
  url = `${environment.api}${EMPLEADO_PREFIX}empleado`;
  
  constructor(
    private http: HttpClient) {
      console.log(this.url);
    }

  public getRegistros(): Observable<EmpleadoInterface[]> {
    return this.http.get<EmpleadoInterface[]>(`${this.url}`);
  }

  public getRegistrosByQuery(params: HttpParams): Observable<EmpleadoInterface[]> {
    return this.http.get<EmpleadoInterface[]>(`${this.url}`, {params});
  }

  public getRegistroById(id: number): Observable<EmpleadoInterface> {
    return this.http.get<EmpleadoInterface>(`${this.url}/${id}`);
  }

  public getRegistroByIdByQuery(id: number, params: HttpParams): Observable<EmpleadoInterface> {
    return this.http.get<EmpleadoInterface>(`${this.url}/${id}`, {params});
  }

  public postRegistro(registro: EmpleadoInterface): Observable<EmpleadoInterface> {
    return this.http.post<EmpleadoInterface>(`${this.url}`, registro);
  }

  public patchRegistro(registro: EmpleadoInterface): Observable<EmpleadoInterface> {
    return this.http.patch<EmpleadoInterface>(`${this.url}/${registro.id}`, registro);
  }

  public postRegistros(registros: EmpleadoInterface[]): Observable<EmpleadoInterface[]> {
    return this.http.post<EmpleadoInterface[]>(`${this.url}/bulk`, {bulk: registros});
  }

  public putRegistro(registro: EmpleadoInterface): Observable<EmpleadoInterface> {
    return this.http.put<EmpleadoInterface>(`${this.url}/${registro.id}`, registro);
  }

  public deleteRegistro(id: number): Observable<EmpleadoInterface> {
    return this.http.delete<EmpleadoInterface>(`${this.url}/${id}`);
  }
}
