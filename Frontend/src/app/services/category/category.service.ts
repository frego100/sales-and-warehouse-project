import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { CATEGORY_PREFIX } from '../../shared/prefix/api.prefix';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CategoryInterface } from '../../interfaces';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  url = `${environment.api}${CATEGORY_PREFIX}`;
  
  constructor(
    private http: HttpClient) {
    }

  public getRegistros(): Observable<CategoryInterface[]> {
    return this.http.get<CategoryInterface[]>(`${this.url}`);
  }

  public getRegistrosByQuery(params: HttpParams): Observable<CategoryInterface[]> {
    return this.http.get<CategoryInterface[]>(`${this.url}`, {params});
  }

  public getRegistroById(id: number): Observable<CategoryInterface> {
    return this.http.get<CategoryInterface>(`${this.url}/${id}`);
  }

  public getRegistroByIdByQuery(id: number, params: HttpParams): Observable<CategoryInterface> {
    return this.http.get<CategoryInterface>(`${this.url}/${id}`, {params});
  }

  public postRegistro(registro: CategoryInterface): Observable<CategoryInterface> {
    return this.http.post<CategoryInterface>(`${this.url}`, registro);
  }

  public patchRegistro(registro: CategoryInterface): Observable<CategoryInterface> {
    return this.http.patch<CategoryInterface>(`${this.url}/${registro.id}`, registro);
  }

  public postRegistros(registros: CategoryInterface[]): Observable<CategoryInterface[]> {
    return this.http.post<CategoryInterface[]>(`${this.url}/bulk`, {bulk: registros});
  }

  public putRegistro(registro: CategoryInterface): Observable<CategoryInterface> {
    return this.http.put<CategoryInterface>(`${this.url}/${registro.id}`, registro);
  }

  public deleteRegistro(id: number): Observable<CategoryInterface> {
    return this.http.delete<CategoryInterface>(`${this.url}/${id}`);
  }
}
