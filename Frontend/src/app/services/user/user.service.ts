import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { USER_PREFIX } from '../../shared/prefix/api.prefix';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserInterface } from '../../interfaces/user/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = `${environment.api}${USER_PREFIX}`;
  
  constructor(
    private http: HttpClient) {
      console.log(this.url);
    }

  public getRegistros(): Observable<UserInterface[]> {
    return this.http.get<UserInterface[]>(`${this.url}`);
  }

  public getRegistrosByQuery(params: HttpParams): Observable<UserInterface[]> {
    return this.http.get<UserInterface[]>(`${this.url}`, {params});
  }

  public getRegistroById(id: number): Observable<UserInterface> {
    return this.http.get<UserInterface>(`${this.url}/${id}`);
  }

  public getRegistroByIdByQuery(id: number, params: HttpParams): Observable<UserInterface> {
    return this.http.get<UserInterface>(`${this.url}/${id}`, {params});
  }

  public postRegistro(registro: UserInterface): Observable<UserInterface> {
    return this.http.post<UserInterface>(`${this.url}`, registro);
  }

  public patchRegistro(registro: UserInterface): Observable<UserInterface> {
    return this.http.patch<UserInterface>(`${this.url}/${registro.id}`, registro);
  }

  public postRegistros(registros: UserInterface[]): Observable<UserInterface[]> {
    return this.http.post<UserInterface[]>(`${this.url}/bulk`, {bulk: registros});
  }

  public putRegistro(registro: UserInterface): Observable<UserInterface> {
    return this.http.put<UserInterface>(`${this.url}/${registro.id}`, registro);
  }

  public deleteRegistro(id: number): Observable<UserInterface> {
    return this.http.delete<UserInterface>(`${this.url}/${id}`);
  }
}
