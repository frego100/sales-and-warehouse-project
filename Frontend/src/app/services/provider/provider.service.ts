import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { PRODUCT_PREFIX } from '../../shared/prefix/api.prefix';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProviderInterface } from '../../interfaces/provider/provider.interface';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  url = `${environment.api}${PRODUCT_PREFIX}`;
  
  constructor(
    private http: HttpClient) {
      console.log(this.url);
    }

  public getRegistros(): Observable<ProviderInterface[]> {
    return this.http.get<ProviderInterface[]>(`${this.url}`);
  }

  public getRegistrosByQuery(params: HttpParams): Observable<ProviderInterface[]> {
    return this.http.get<ProviderInterface[]>(`${this.url}`, {params});
  }

  public getRegistroById(id: number): Observable<ProviderInterface> {
    return this.http.get<ProviderInterface>(`${this.url}/${id}`);
  }

  public getRegistroByIdByQuery(id: number, params: HttpParams): Observable<ProviderInterface> {
    return this.http.get<ProviderInterface>(`${this.url}/${id}`, {params});
  }

  public postRegistro(registro: ProviderInterface): Observable<ProviderInterface> {
    return this.http.post<ProviderInterface>(`${this.url}`, registro);
  }

  public patchRegistro(registro: ProviderInterface): Observable<ProviderInterface> {
    return this.http.patch<ProviderInterface>(`${this.url}/${registro.id}`, registro);
  }

  public postRegistros(registros: ProviderInterface[]): Observable<ProviderInterface[]> {
    return this.http.post<ProviderInterface[]>(`${this.url}/bulk`, {bulk: registros});
  }

  public putRegistro(registro: ProviderInterface): Observable<ProviderInterface> {
    return this.http.put<ProviderInterface>(`${this.url}/${registro.id}`, registro);
  }

  public deleteRegistro(id: number): Observable<ProviderInterface> {
    return this.http.delete<ProviderInterface>(`${this.url}/${id}`);
  }
}
