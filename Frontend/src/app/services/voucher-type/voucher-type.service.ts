import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import {  VOUCHER_TYPE_PREFIX } from '../../shared/prefix/api.prefix';
import { HttpClient, HttpParams } from '@angular/common/http';
import { VoucherTypeInterface } from '../../interfaces';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class VoucherTypeService {
  url = `${environment.api}${VOUCHER_TYPE_PREFIX}`;
  constructor(
    private http: HttpClient) {
      console.log(this.url);
    }

  public getRegistros(): Observable<VoucherTypeInterface[]> {
    return this.http.get<VoucherTypeInterface[]>(`${this.url}`);
  }

  public getRegistrosByQuery(params: HttpParams): Observable<VoucherTypeInterface[]> {
    return this.http.get<VoucherTypeInterface[]>(`${this.url}`, {params});
  }

  public getRegistroById(id: number): Observable<VoucherTypeInterface> {
    return this.http.get<VoucherTypeInterface>(`${this.url}/${id}`);
  }

  public getRegistroByIdByQuery(id: number, params: HttpParams): Observable<VoucherTypeInterface> {
    return this.http.get<VoucherTypeInterface>(`${this.url}/${id}`, {params});
  }

  public postRegistro(registro: VoucherTypeInterface): Observable<VoucherTypeInterface> {
    return this.http.post<VoucherTypeInterface>(`${this.url}`, registro);
  }

  public patchRegistro(registro: VoucherTypeInterface): Observable<VoucherTypeInterface> {
    return this.http.patch<VoucherTypeInterface>(`${this.url}/${registro.id}`, registro);
  }

  public postRegistros(registros: VoucherTypeInterface[]): Observable<VoucherTypeInterface[]> {
    return this.http.post<VoucherTypeInterface[]>(`${this.url}/bulk`, {bulk: registros});
  }

  public putRegistro(registro: VoucherTypeInterface): Observable<VoucherTypeInterface> {
    return this.http.put<VoucherTypeInterface>(`${this.url}/${registro.id}`, registro);
  }

  public deleteRegistro(id: number): Observable<VoucherTypeInterface> {
    return this.http.delete<VoucherTypeInterface>(`${this.url}/${id}`);
  }
}
