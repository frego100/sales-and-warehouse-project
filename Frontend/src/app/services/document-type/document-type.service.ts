import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { DOCUMENT_TYPE_PREFIX } from '../../shared/prefix/api.prefix';
import { HttpClient, HttpParams } from '@angular/common/http';
import { DocumentTypeInterface } from '../../interfaces';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DocumentTypeService {
  url = `${environment.api}${DOCUMENT_TYPE_PREFIX}`;
  constructor(
    private http: HttpClient) {
      console.log(this.url);
    }

  public getRegistros(): Observable<DocumentTypeInterface[]> {
    return this.http.get<DocumentTypeInterface[]>(`${this.url}`);
  }

  public getRegistrosByQuery(params: HttpParams): Observable<DocumentTypeInterface[]> {
    return this.http.get<DocumentTypeInterface[]>(`${this.url}`, {params});
  }

  public getRegistroById(id: number): Observable<DocumentTypeInterface> {
    return this.http.get<DocumentTypeInterface>(`${this.url}/${id}`);
  }

  public getRegistroByIdByQuery(id: number, params: HttpParams): Observable<DocumentTypeInterface> {
    return this.http.get<DocumentTypeInterface>(`${this.url}/${id}`, {params});
  }

  public postRegistro(registro: DocumentTypeInterface): Observable<DocumentTypeInterface> {
    return this.http.post<DocumentTypeInterface>(`${this.url}`, registro);
  }

  public patchRegistro(registro: DocumentTypeInterface): Observable<DocumentTypeInterface> {
    return this.http.patch<DocumentTypeInterface>(`${this.url}/${registro.id}`, registro);
  }

  public postRegistros(registros: DocumentTypeInterface[]): Observable<DocumentTypeInterface[]> {
    return this.http.post<DocumentTypeInterface[]>(`${this.url}/bulk`, {bulk: registros});
  }

  public putRegistro(registro: DocumentTypeInterface): Observable<DocumentTypeInterface> {
    return this.http.put<DocumentTypeInterface>(`${this.url}/${registro.id}`, registro);
  }

  public deleteRegistro(id: number): Observable<DocumentTypeInterface> {
    return this.http.delete<DocumentTypeInterface>(`${this.url}/${id}`);
  }
}
