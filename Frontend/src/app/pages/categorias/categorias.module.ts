import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriasRoutingModule } from './categorias-routing.module';
import { CategoriasComponent } from './categorias/categorias.component';
import { LibsModule } from '../../libs/libs.module';
import { DocumentTypeComponent } from './document-type/document-type.component';
import { VoucherTypeComponent } from './voucher-type/voucher-type.component';


@NgModule({
  declarations: [CategoriasComponent, DocumentTypeComponent, VoucherTypeComponent],
  imports: [
    CommonModule,
    CategoriasRoutingModule,
    LibsModule
  ]
})
export class CategoriasModule { }
