import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { RequestQueryBuilder } from '@nestjsx/crud-request';
import { VoucherTypeService } from '../../../services';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'ngx-voucher-type',
  templateUrl: './voucher-type.component.html',
  styleUrls: ['./voucher-type.component.scss']
})
export class VoucherTypeComponent implements OnInit {
  categories:any[]=[];
  newCategory:any;
  ngOnInit(): void {
    this.getCategories();
  }
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave:true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable:false,
      },
      vname: {
        title: 'Nombre',
        type: 'string',
      },
      description: {
        title: 'Descripción',
        type: 'string',
      },
      createdat: {
        title: 'F.Creación',
        type: 'string',
        editable:false,
        hideHeader:true
      },
      updatedat: {
        title: 'F.Actualización',
        type: 'string',
        editable:false,	
        hideHeader:true
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData,private voucherTypeService:VoucherTypeService) {
    const data = this.service.getData();
    //this.source.load(data);
  }
  getCategories(){
    const queryString=RequestQueryBuilder
    .create()
    .query();
    const params=new HttpParams({fromString:queryString});

    const datePipe = new DatePipe('en-US');

    this.voucherTypeService.getRegistrosByQuery(params).subscribe(
      data=>{
        console.log("my data",data);
        if(data.length===0){
          this.categories=[];
        }
        else{
          this.categories=data.map(x=>({
            id:x.id,
            vname:x.vname,
            description:x.description,
            createdat:datePipe.transform(x.createdat,'M/d/yy, h:mm a'),
            updatedat:datePipe.transform(x.updatedat,'M/d/yy, h:mm a')
          }));
        }
        this.source.load(this.categories);
      },
      error=>{
        console.log('Failed to get');
      }
    )
  }
  async onCreateConfirm(event){
    this.newCategory={
      vname:event.newData.vname,
      description:event.newData.description
    }
    console.log(this.newCategory);  
    this.voucherTypeService.postRegistros([this.newCategory]).subscribe(
      data=>{
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 5000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'El registro se creo correctamente'
        })
        this.getCategories();
        event.confirm.resolve();
      },
      error=>{
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'warning',
          title: 'No se pudo crear , Intentalo de nuevo'
        })
        event.confirm.reject();
      }
    )
  
  }
  onDeleteConfirm(event): void {
    const category=event.data;
    console.log(event);
    Swal.fire({
      title: 'Estas seguro?',
      text: "No podrás revertir esto!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borrar!'
    }).then((result) => {
      if (result.value) {
        this.voucherTypeService.deleteRegistro(category.id).subscribe(
          data=>{
            event.confirm.resolve();
            Swal.fire(
              'Eliminado!',
              'Su registro ha sido eliminado.',
              'success'
            )
          },
          error=>{
            event.confirm.reject();
            Swal.fire(
              'No se pudo eliminar',
              'Ocurrio un error al intentar eliminar',
              'warning'
            )
          }
        );
      }
    })
  }
  onSaveConfirm(event):void{
    this.newCategory={
      id:event.newData.id,
      vname:event.newData.vname,
      description:event.newData.description
    }
    console.log(this.newCategory);  
    this.voucherTypeService.postRegistros([this.newCategory]).subscribe(
      data=>{
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'El registro se creo correctamente'
        })
        this.getCategories();
        event.confirm.resolve();
      },
      error=>{
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'warning',
          title: 'No se pudo crear , Intentalo de nuevo'
        })
        event.confirm.reject();
      }
    )
  }
  onRowSelect(event){
    
  }


}
