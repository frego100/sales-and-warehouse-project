import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriasComponent } from './categorias/categorias.component';
import { DocumentTypeComponent } from './document-type/document-type.component';
import { VoucherTypeComponent } from './voucher-type/voucher-type.component';


const routes: Routes = [
  {
    path:'',
    component:CategoriasComponent
  },
  {
    path:'document-type',
    component:DocumentTypeComponent
  },
  { 
    path:'voucher-type',
    component:VoucherTypeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriasRoutingModule { }
