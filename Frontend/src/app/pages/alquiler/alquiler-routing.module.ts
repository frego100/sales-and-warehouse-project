import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlquilerComponent } from './alquiler/alquiler.component';


const routes: Routes = [
  {
    path:'',
    component:AlquilerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlquilerRoutingModule { }
