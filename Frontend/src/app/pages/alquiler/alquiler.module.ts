import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlquilerRoutingModule } from './alquiler-routing.module';
import { AlquilerComponent } from './alquiler/alquiler.component';
import { LibsModule } from '../../libs/libs.module';


@NgModule({
  declarations: [AlquilerComponent],
  imports: [
    CommonModule,
    AlquilerRoutingModule,
    LibsModule
  ]
})
export class AlquilerModule { }
