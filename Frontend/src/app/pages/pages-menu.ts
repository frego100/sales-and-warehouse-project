import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'grid-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Modulos',
    group: true,
  },
  {
    title: 'Modulo Empleados',
    icon: 'people-outline',
    children: [
      {
        title: 'Empleados',
        link: '/pages/empleados',
      },
    ],
  },
  {
    title: 'Modulo Usuarios',
    icon: 'person-done-outline',
    children: [
      {
        title: 'Usuarios',
        link: '/pages/usuarios',
      },
    ],
  },
  {
    title: 'Modulo Alquiler',
    icon: 'flip-outline',
    children: [
      {
        title: 'Alquiler',
        link: '/pages/alquiler',
      },
    ],
  },
  {
    title: 'Modulo Productos',
    icon: 'archive-outline',
    children: [
      {
        title: 'Productos',
        link: '/pages/productos',
      },
    ],
  },
  {
    title: 'Modulo Proveedores',
    icon: 'person-add-outline',
    children: [
      {
        title: 'Provehedores',
        link: '/pages/provehedores',
      },
    ],
  },
  {
    title: 'Modulo Ventas',
    icon: 'shopping-cart-outline',
    children: [
      {
        title: 'Ventas',
        link: '/pages/ventas',
      },
    ],
  },
  {
    title: 'Modulo Clientes',
    icon: 'person-outline',
    children: [
      {
        title: 'Clientes',
        link: '/pages/clientes',
      }
    ],
  },
  {
    title: 'Modulo Documentos',
    icon: 'list-outline',
    children: [
      {
        title: 'Categorias',
        link: '/pages/categorias',
      },
      {
        title:'Tipos de documento',
        link:'/pages/categorias/document-type'
      },      
      {
        title:'Tipos de comprobante',
        link:'/pages/categorias/voucher-type'     
      }
    ],
  },
];
