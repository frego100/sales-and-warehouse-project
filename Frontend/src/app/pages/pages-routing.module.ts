import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
//import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './dashboard/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: ECommerceComponent,
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path:'empleados',
      loadChildren:()=>import('./empleados/empleados.module')
        .then(m=>m.EmpleadosModule)
    },
    {
      path:'usuarios',
      loadChildren:()=>import('./usuarios/usuarios.module')
        .then(m=>m.UsuariosModule)
    },
    {
      path:'alquiler',
      loadChildren:()=>import('./alquiler/alquiler.module')
        .then(m=>m.AlquilerModule)
    },
    {
      path:'productos',
      loadChildren:()=>import('./productos/productos.module')
        .then(m=>m.ProductosModule)
    },
    {
      path:'provehedores',
      loadChildren:()=>import('./provehedores/provehedores.module')
        .then(m=>m.ProvehedoresModule)
    },
    {
      path:'ventas',
      loadChildren:()=>import('./ventas/ventas.module')
        .then(m=>m.VentasModule)
    },
    {
      path:'clientes',
      loadChildren:()=>import('./clientes/clientes.module')
        .then(m=>m.ClientesModule)
    },
    {
      path:'categorias',
      loadChildren:()=>import('./categorias/categorias.module')
        .then(m=>m.CategoriasModule)
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
