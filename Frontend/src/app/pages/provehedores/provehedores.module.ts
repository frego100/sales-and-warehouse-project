import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProvehedoresRoutingModule } from './provehedores-routing.module';
import { ProvehedoresComponent } from './provehedores/provehedores.component';
import { LibsModule } from '../../libs/libs.module';


@NgModule({
  declarations: [ProvehedoresComponent],
  imports: [
    CommonModule,
    ProvehedoresRoutingModule,
    LibsModule
  ]
})
export class ProvehedoresModule { }
