import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvehedoresComponent } from './provehedores.component';

describe('ProvehedoresComponent', () => {
  let component: ProvehedoresComponent;
  let fixture: ComponentFixture<ProvehedoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProvehedoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvehedoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
