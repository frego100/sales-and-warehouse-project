import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';

@Component({
  selector: 'ngx-provehedores',
  templateUrl: './provehedores.component.html',
  styleUrls: ['./provehedores.component.scss']
})
export class ProvehedoresComponent implements OnInit {
  ngOnInit(): void {
  }
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate:true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave:true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      pname: {
        title: 'Nombre',
        type: 'string',
      },
      documentType: {
        title: 'Tipo de documento',
        type: 'string',
      },
      documentNumber: {
        title: 'N° Documento',
        type: 'string',
      },
      ruc: {
        title: 'RUC',
        type: 'string',
      },
      email: {
        title: 'Correo',
        type: 'string',
      },
      address: {
        title: 'Direccion',
        type: 'string',
      },
      phoneNumber: {
        title: 'Numero Celular',
        type: 'string',
      },
      city: {
        title: 'Ciudad',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData) {
    const data = this.service.getData();
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }


}
