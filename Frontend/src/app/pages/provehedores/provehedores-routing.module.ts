import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProvehedoresComponent } from './provehedores/provehedores.component';


const routes: Routes = [
  {
    path:'',
    component:ProvehedoresComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProvehedoresRoutingModule { }
