import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpleadosRoutingModule } from './empleados-routing.module';
import { EmpleadosComponent } from './empleados/empleados.component';
import { LibsModule } from '../../libs/libs.module';
import { FormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { SmartTableDatepickerComponent, SmartTableDatepickerRenderComponent } from '../../shared/components/date-picker/date-picker.component';


@NgModule({
  declarations: [
    EmpleadosComponent,
    SmartTableDatepickerComponent,
    SmartTableDatepickerRenderComponent
  ],
  imports: [
    CommonModule,
    EmpleadosRoutingModule,
    LibsModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  entryComponents:[
    SmartTableDatepickerComponent,
    SmartTableDatepickerRenderComponent  
  ]
})

export class EmpleadosModule { }
