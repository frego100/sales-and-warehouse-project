import { Component, OnInit, Input } from "@angular/core";
import { LocalDataSource, ViewCell } from "ng2-smart-table";
import { SmartTableData } from "../../../@core/data/smart-table";
import { EmpleadoService } from "./../../../services/empleado/empleado.service";
import { RequestQueryBuilder, CondOperator } from "@nestjsx/crud-request";
import { HttpParams } from "@angular/common/http";
import Swal from "sweetalert2";
import { EmpleadoInterface } from "../../../interfaces";
import { EmployeService } from "../../../services/employe/employe.service";
import { DatePipe } from "@angular/common";
import { title } from "process";
import {
  SmartTableDatepickerRenderComponent,
  SmartTableDatepickerComponent,
} from "../../../shared/components/date-picker/date-picker.component";
import { DocumentTypeService } from "../../../services";
import { Observable, observable } from "rxjs";

@Component({
  selector: "ngx-empleados",
  templateUrl: "./empleados.component.html",
  styleUrls: ["./empleados.component.scss"],
})
export class EmpleadosComponent implements OnInit {
  empleados: any[] = [];
  documentTypeL: any[] = [];
  newEmploye: any;
  settings = {};
  source: LocalDataSource = new LocalDataSource();
  async ngOnInit() {
    this.getRegistros();
    this.getdocumentTypeRegister().subscribe((res) => {
      this.settings = this.getnewSettings();
    });
  }
  constructor(
    private service: SmartTableData,
    private empleadoService: EmployeService,
    private documentTypeService: DocumentTypeService
  ) {
  }
  getnewSettings() {
    return {
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true,
      },
      add: {
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true,
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash"></i>',
        confirmDelete: true,
      },
      columns: {
        id: {
          title: 'ID',
          type: 'number',
          editable:false,
        },
        ename: {
          title: "Nombre",
          type: "string",
        },
        documentType: {
          title: "Tipo de documento",
          editor: {
            type: "list",
            config: {
              selectText: "Select",
              list: this.documentTypeL,
            },
            filter: {
              type: "list",
              config: {
                selectText: "Select",
                list: this.documentTypeL,
              },
            },
          },
        },
        documentNumber: {
          title: "N°.Documento",
          type: "string",
        },
        email: {
          title: "Correo",
          type: "string",
        },
        address: {
          title: "Direccion",
          type: "string",
        },
        jobName: {
          title: "Cargo",
          type: "String",
        },
        phoneNumber: {
          title: "Numero Telefono",
          type: "string",
        },
        city: {
          title: "Ciudad",
          type: "string",
        },
        admissionDate: {
          title: "F.Ingreso",
          type: "custom",
          renderComponent: SmartTableDatepickerRenderComponent,
          //width: '250px',
          //filter: false,
          //sortDirection: 'desc',
          editor: {
            type: "custom",
            component: SmartTableDatepickerComponent,
          },
        },
        birthDate: {
          title: "Fecha Nacimiento",
          type: "custom",
          renderComponent: SmartTableDatepickerRenderComponent,
          //width: '250px',
          //filter: false,
          //sortDirection: 'desc',
          editor: {
            type: "custom",
            component: SmartTableDatepickerComponent,
          },
        },
        createdat: {
          title: "F.Creación",
          type: "string",
          editable: false,
          hideHeader: true,
        },
        updatedat: {
          title: "F.Actualización",
          type: "string",
          editable: false,
          hideHeader: true,
        },
      },
    };
  }
  getdocumentTypeRegister() {
    const queryString = RequestQueryBuilder.create().query();
    const params = new HttpParams({ fromString: queryString });
    return Observable.create((observable) => {
      this.documentTypeService.getRegistrosByQuery(params).subscribe((data) => {
        if (data.length === 0) {
          this.documentTypeL = [];
        } else {
          this.documentTypeL = data.map((x) => ({
            value: x.id,
            title: x.description,
          }));
        }
        observable.next(true);
        return observable.complete();
      });
    });
  }
  getRegistros() {
    const queryString = RequestQueryBuilder.create().query();
    const datePipe = new DatePipe("en-US");
    const params = new HttpParams({ fromString: queryString });
    this.empleadoService.getRegistrosByQuery(params).subscribe(
      (data) => {
        if (data.length === 0) {
          this.empleados = [];
        } else {
          this.empleados = data.map((x) => ({
            id: x.id,
            ename: x.ename,
            documentNumber: x.documentNumber,
            email: x.email,
            address: x.address,
            phoneNumber: x.phoneNumber,
            city: x.city,
            admissionDate: x.admissionDate,
            birthDate: x.birthDate,
            jobName: x.jobName,
            documentType: x.documentType.description,
            createdat: datePipe.transform(x.createdat, "M/d/yy, h:mm a"),
            updatedat: datePipe.transform(x.updatedat, "M/d/yy, h:mm a"),
          }));
        }
        this.source.load(this.empleados);
      },
      (error) => {
        console.log("falied to get ");
      }
    );
  }
  async onCreateConfirm(event) {
    console.log(event.newData);
    this.newEmploye = {
      ename: event.newData.ename,
      documentNumber: event.newData.documentNumber,
      email: event.newData.email,
      address: event.newData.address,
      phoneNumber: event.newData.phoneNumber,
      city: event.newData.city,
      admissionDate: event.newData.admissionDate,
      birthDate: event.newData.birthDate,
      jobName: event.newData.jobName,
      documentType: event.newData.documentType
    };
    //console.log(this.newEmploye);
    this.empleadoService.postRegistros([this.newEmploye]).subscribe(
      (data) => {
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 5000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener("mouseenter", Swal.stopTimer);
            toast.addEventListener("mouseleave", Swal.resumeTimer);
          },
        });
        Toast.fire({
          icon: "success",
          title: "El registro se creo correctamente",
        });
        this.getRegistros();
        event.confirm.resolve();
      },
      (error) => {
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener("mouseenter", Swal.stopTimer);
            toast.addEventListener("mouseleave", Swal.resumeTimer);
          },
        });
        Toast.fire({
          icon: "warning",
          title: "No se pudo crear , Intentalo de nuevo",
        });
        event.confirm.reject();
      }
    );
  }
  onDeleteConfirm(event): void {
    const category = event.data;
    console.log(event);
    Swal.fire({
      title: "Estas seguro?",
      text: "No podrás revertir esto!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, borrar!",
    }).then((result) => {
      if (result.value) {
        this.empleadoService.deleteRegistro(category.id).subscribe(
          (data) => {
            event.confirm.resolve();
            Swal.fire(
              "Eliminado!",
              "Su registro ha sido eliminado.",
              "success"
            );
          },
          (error) => {
            event.confirm.reject();
            Swal.fire(
              "No se pudo eliminar",
              "Ocurrio un error al intentar eliminar",
              "warning"
            );
          }
        );
      }
    });
  }

  onSaveConfirm(event): void {
    console.log(event.newData)
    this.newEmploye = {
      id:event.newData.id,
      ename: event.newData.ename,
      documentNumber: event.newData.documentNumber,
      email: event.newData.email,
      address: event.newData.address,
      phoneNumber: event.newData.phoneNumber,
      city: event.newData.city,
      admissionDate: event.newData.admissionDate,
      birthDate: event.newData.birthDate,
      jobName: event.newData.jobName,
      documentType: event.newData.documentType
    };
    console.log(this.newEmploye);  
    this.empleadoService.postRegistros([this.newEmploye]).subscribe(
      data=>{
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'El registro se creo correctamente'
        })
        this.getRegistros();
        event.confirm.resolve();
      },
      error=>{
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'warning',
          title: 'No se pudo crear , Intentalo de nuevo'
        })
        event.confirm.reject();
      }
    )
  }
  onRowSelect(event) {}
}
