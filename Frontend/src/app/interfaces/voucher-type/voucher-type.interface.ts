import { BaseInterface } from '../../shared/interfaces';
export interface VoucherTypeInterface extends BaseInterface{
    description:string;
    vname:string;
}