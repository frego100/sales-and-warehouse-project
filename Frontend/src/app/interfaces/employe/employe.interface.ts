import { BaseInterface } from "../../shared/interfaces";
import { DocumentTypeInterface } from '../document-type/document-type.interface';
export interface EmployeInterface extends BaseInterface {
  ename:String;
  documentNumber:number;
  email: string;
  address: string;
  phoneNumber: number;
  city: string;
  admissionDate: Date;
  birthDate: Date;
  jobName: string;
  documentType: DocumentTypeInterface
}
