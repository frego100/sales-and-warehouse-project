import { BaseInterface } from "../../shared/interfaces";
import { TrabajoInterface } from "../trabajo/trabajo.interface";
import { CategoryInterface } from '../category/category.interface';
export interface ProductsInterface extends BaseInterface {
    pname: string;
    model: string;
    desciption: string;
    price: number;
    stock: number;
    category: CategoryInterface;
}
