import { BaseInterface } from '../../shared/interfaces';
export interface DocumentTypeInterface extends BaseInterface{
    description:string;
}