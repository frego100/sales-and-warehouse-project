import { BaseInterface } from '../../shared/interfaces';
import { TrabajoInterface } from '../trabajo/trabajo.interface';
export interface EmpleadoInterface extends BaseInterface {
    nombres:string,
    apellidoMaterno:string,
    apellidoPaterno:string,
    dni:string,
    estado:boolean,
    correo:string,
    direccion:string,
    numTelefono:String,
    fechaNacimiento:Date,
    fechaIngreso:Date,
    idTrabajo:TrabajoInterface
}

