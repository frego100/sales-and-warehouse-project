import { BaseInterface } from "../../shared/interfaces";
import { DocumentTypeInterface } from '../document-type/document-type.interface';
export interface ProviderInterface extends BaseInterface {
    pname: string;
    documentNumber: string;
    ruc: string;
    email: string;
    address: string;
    phoneNumber: string;
    city: string;
    documentType: DocumentTypeInterface;
}
