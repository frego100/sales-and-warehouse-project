import { BaseInterface } from '../../shared/interfaces';
export interface CategoryInterface extends BaseInterface{
    cname?:string;
    description?:string;
}