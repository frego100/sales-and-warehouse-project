import { BaseInterface } from "../../shared/interfaces";
import { EmployeInterface } from '../employe/employe.interface';
export interface UserInterface extends BaseInterface {
    email: string;
    upassword: string;
    employe:EmployeInterface
}
