import { BaseInterface } from '../../shared/interfaces';
export interface TrabajoInterface extends BaseInterface{
    descripcion?:string;
    salario?:number;
}
