import { PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';

export class BaseEntity {
    @PrimaryGeneratedColumn({ type: "integer", name: "id" })
    id: number;  
    @CreateDateColumn({nullable:true})
    createdat?:Date;
    @CreateDateColumn({nullable:true})
    updatedat?:Date;
}