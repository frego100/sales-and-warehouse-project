import * as dotenv from 'dotenv';

dotenv.config();
let entorno: string;

switch (process.env.NODE_ENV) {
  case 'development':
    entorno = `${__dirname}/.env.development`;
    break;
  case 'production':
    entorno = `${__dirname}/.env.production`;
    break;
  default:
    entorno = `${__dirname}/.env.development`;
}
dotenv.config({ path: entorno });

export const config = {
    database: {
      ssl: process.env.DATABASE_SSL ? process.env.DATABASE_SSL.toLowerCase() === 'true' : true,
      uri: process.env.DATABASE_URL ? process.env.DATABASE_URL : 'postgres://cbhkvdnadfuvtf:0e5167c568068ea26d7ab93711b0cc45d40e61f8bbf9935d52ee02fc047d820a@ec2-50-17-90-177.compute-1.amazonaws.com:5432/de6v24ejjudj38',
      entities: process.env.ENTITIES ? process.env.ENTITIES : '/../**/*.entity.js'
    },
};
