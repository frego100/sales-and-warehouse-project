import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { config } from './common/config/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClienteModule } from './modules/cliente/cliente.module';
import { EmpleadoModule } from './modules/empleado/empleado.module';
import { TrabajoModule } from './modules/trabajo/trabajo.module';
import { EmployeModule } from './modules/employe/employe.module';
import { CategoryModule } from './modules/category/category.module';
import { SharedModuleModule } from './modules/shared-module/shared-module.module';
import { ProvidersModule } from './modules/providers/providers.module';
import { UsersModule } from './modules/users/users.module';
import { ProductsModule } from './modules/products/products.module';
@Module({
  imports: [
    TypeOrmModule.forRoot({
    type:'postgres',
    url:config.database.uri,
    ssl:config.database.ssl,
    synchronize:false,
    extra:{
      ssl:true
    },
    logging:true,
    entities: [__dirname + config.database.entities]
  }), ClienteModule, EmpleadoModule, TrabajoModule, EmployeModule, CategoryModule,SharedModuleModule, ProvidersModule, UsersModule, ProductsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
