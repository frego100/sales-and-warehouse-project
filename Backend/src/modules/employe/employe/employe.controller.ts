import { Controller } from '@nestjs/common';
//import { ApiUseTags } from '@nestjs/swagger';
import {
  Crud,
  CrudController,
  Override,
  ParsedRequest,
  CrudRequest
} from '@nestjsx/crud';
import { API_PREFIX,EMPLEADO_PREFIX, EMPLOYE_PREFIX } from 'src/common/constants/routes';
import { Employe } from './employe.entity';
import { EmployeService } from './employe.service';
@Crud({
    model: {
      type: Employe,
    },
    params: {
      id: {
        field: 'id',
        type: 'number',
        primary: true,
      },
    },
    query:{
      join:{
        documentType:{
          eager:true,
          required:false,
          exclude:['createdat','updatedat']
        }
      }
    }
  })
@Controller(`${API_PREFIX}${EMPLOYE_PREFIX}employe`)
export class EmployeController implements CrudController<Employe> {
    constructor(public service:EmployeService){};
    get base():CrudController<Employe>{
        return this;
    }
    @Override('getManyBase')
    getAll(@ParsedRequest() req:CrudRequest){
        return this.base.getManyBase(req);
    }
}
