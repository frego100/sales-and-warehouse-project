import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { DocumentType } from './../../shared-module/document-type/document-type.entity';
import { BaseEntity } from 'src/common/entities';
import { Users } from './../../users/user.entity';
@Index('employe_document_number_key', ['documentNumber'], { unique: true })
@Entity('employe', { schema: 'public' })
@Index('employe_email_key', ['email'], { unique: true })
@Index('employe_pkey', ['id'], { unique: true })
@Index('employe_phone_number_key', ['phoneNumber'], { unique: true })
export class Employe extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @Column('character varying', { name: 'ename', length: 200 })
  ename: string;

  @Column('character', { name: 'document_number', unique: true, length: 25 })
  documentNumber: string;

  @Column('character varying', { name: 'email', unique: true, length: 200 })
  email: string;

  @Column('character varying', { name: 'address', nullable: true, length: 255 })
  address: string | null;

  @Column('character varying', {
    name: 'phone_number',
    unique: true,
    length: 25,
  })
  phoneNumber: string;

  @Column('character varying', { name: 'city', nullable: true, length: 25 })
  city: string | null;

  @Column('date', { name: 'admission_date' })
  admissionDate: string;

  @Column('date', { name: 'birth_date' })
  birthDate: string;

  @Column('character varying', { name: 'job_name', length: 25 })
  jobName: string;

  @Column('timestamp without time zone', {
    name: 'createdat',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdat: Date;

  @Column('timestamp without time zone', {
    name: 'updatedat',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedat: Date;

  @ManyToOne(
    () => DocumentType,
    documentType => documentType.employes,
  )
  @JoinColumn([{ name: 'document_type_id', referencedColumnName: 'id' }])
  documentType: DocumentType;

  @OneToMany(
    () => Users,
    users => users.employe,
  )
  users: Users[];
}
