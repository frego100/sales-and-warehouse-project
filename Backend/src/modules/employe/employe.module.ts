import { Module } from '@nestjs/common';
import { EmployeService } from './employe/employe.service';
import { EmployeController } from './employe/employe.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employe } from './employe/employe.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Employe
    ])
  ],
  exports:[
    EmployeService
  ],
  providers: [
    EmployeService
  ],
  controllers: [
    EmployeController
  ]
})
export class EmployeModule {

}
