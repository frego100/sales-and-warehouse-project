import { Module } from '@nestjs/common';
import { TrabajoController } from './trabajo/trabajo.controller';
import { TrabajoService } from './trabajo/trabajo.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Trabajo } from './trabajo/trabajo.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Trabajo
    ])
  ],
  exports:[
    TrabajoService
  ],
  controllers: [
    TrabajoController
  ],
  providers: [
    TrabajoService
  ]
})
export class TrabajoModule {}
