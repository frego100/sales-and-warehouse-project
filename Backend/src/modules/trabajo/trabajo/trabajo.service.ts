import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Trabajo } from './trabajo.entity';

@Injectable()
export class TrabajoService extends TypeOrmCrudService<Trabajo> {
    constructor(@InjectRepository(Trabajo)repo){
        super(repo)
    }
}
