import { Controller } from '@nestjs/common';
import {
    Crud,
    CrudController
  } from '@nestjsx/crud';
  import { API_PREFIX,TRABAJO_PREFIX } from 'src/common/constants/routes';
  import { Trabajo } from './trabajo.entity';
  import { TrabajoService } from './trabajo.service';
  @Crud({
      model: {
        type: Trabajo,
      },
      params: {
        id: {
          field: 'id',
          type: 'number',
          primary: true,
        },
      }
    })
  
//@Controller('trabajo')
@Controller(`${API_PREFIX}${TRABAJO_PREFIX}trabajo`)
export class TrabajoController implements CrudController<Trabajo> {
    constructor(public service:TrabajoService){};
}
