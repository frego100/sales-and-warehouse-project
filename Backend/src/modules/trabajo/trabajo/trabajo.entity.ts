import {
    Column,
    Entity,
    Index,
    OneToMany,
    PrimaryGeneratedColumn,
  } from "typeorm";
  import { Empleado } from "./../../empleado/empleado/empleado.entity";
import { BaseEntity } from "src/common/entities";
  
  @Index("trabajo_pkey", ["id"], { unique: true })
  @Entity("trabajo", { schema: "public" })
  export class Trabajo extends BaseEntity{
    @PrimaryGeneratedColumn({ type: "integer", name: "id" })
    id: number;
  
    @Column("character varying", { name: "descripcion", length: 200 })
    descripcion: string;
  
    @Column("numeric", { name: "salario" })
    salario: string;
  
    @Column("timestamp without time zone", {
      name: "createdat",
      default: () => "CURRENT_TIMESTAMP",
    })
    createdat: Date;
  
    @Column("timestamp without time zone", {
      name: "updatedat",
      default: () => "CURRENT_TIMESTAMP",
    })
    updatedat: Date;
  
    @OneToMany(() => Empleado, (empleado) => empleado.idTrabajo)
    empleados: Empleado[];
  }