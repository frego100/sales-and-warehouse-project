import { Test, TestingModule } from '@nestjs/testing';
import { TrabajoController } from './trabajo.controller';

describe('Trabajo Controller', () => {
  let controller: TrabajoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TrabajoController],
    }).compile();

    controller = module.get<TrabajoController>(TrabajoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
