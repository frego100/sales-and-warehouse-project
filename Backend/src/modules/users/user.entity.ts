import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Employe } from './../employe/employe/employe.entity';

@Index('users_email_key', ['email'], { unique: true })
@Index('users_pkey', ['id'], { unique: true })
@Entity('users', { schema: 'public' })
export class Users {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @Column('character varying', { name: 'email', unique: true, length: 200 })
  email: string;

  @Column('character', { name: 'upassword', length: 25 })
  upassword: string;

  @Column('timestamp without time zone', {
    name: 'createdat',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdat: Date;

  @Column('timestamp without time zone', {
    name: 'updatedat',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedat: Date;

  @ManyToOne(
    () => Employe,
    employe => employe.users,
  )
  @JoinColumn([{ name: 'employe_id', referencedColumnName: 'id' }])
  employe: Employe;
}
