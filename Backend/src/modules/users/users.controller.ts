import { Controller } from '@nestjs/common';
//import { ApiUseTags } from '@nestjs/swagger';
import { Crud, CrudController, Override, ParsedRequest, CrudRequest } from '@nestjsx/crud';
import { API_PREFIX,USER_PREFIX } from 'src/common/constants/routes';
import { Users } from './user.entity';
import { UsersService } from './users.service';
@Crud({
  model: {
    type: Users,
  },
  params: {
    id: {
      field: 'id',
      type: 'number',
      primary: true,
    },
  },
  query:{
      join:{
        employe:{
            eager:true,
            required:false,
            exclude:['createdat','updatedat']
        }
      }
  }
})
@Controller(`${API_PREFIX}${USER_PREFIX}user`)
export class UsersController implements CrudController<Users> {
    constructor(public service:UsersService){};
    get base():CrudController<Users>{
        return this;
    }
    @Override('getManyBase')
    getAll(@ParsedRequest() req:CrudRequest){
        return this.base.getManyBase(req);
    }
}
