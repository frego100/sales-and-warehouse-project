import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { DocumentType } from './document-type.entity';

@Injectable()
export class DocumentTypeService extends TypeOrmCrudService<DocumentType> {
    constructor(@InjectRepository(DocumentType) repo){
        super(repo);
    }
}
