import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Employe } from './../../employe/employe/employe.entity';
import { BaseEntity } from 'src/common/entities';
import { Providers } from './../../providers/providers/provider.entity';
@Index('document_type_pkey', ['id'], { unique: true })
@Entity('document_type', { schema: 'public' })
export class DocumentType extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @Column('character varying', { name: 'description', length: 200 })
  description: string;

  @Column('timestamp without time zone', {
    name: 'createdat',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdat: Date;

  @Column('timestamp without time zone', {
    name: 'updatedat',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedat: Date;

  @OneToMany(
    () => Employe,
    employe => employe.documentType,
  )
  employes: Employe[];

  @OneToMany(
    () => Providers,
    providers => providers.documentType,
  )
  providers: Providers[];
}
