import { Controller } from '@nestjs/common';
//import { ApiUseTags } from '@nestjs/swagger';
import {
  Crud,
  CrudController
} from '@nestjsx/crud';
import { API_PREFIX,DOCUMENT_TYPE } from 'src/common/constants/routes';
import { DocumentType } from './document-type.entity';
import { DocumentTypeService } from './document-type.service';
@Crud({
    model: {
      type: DocumentType,
    },
    params: {
      id: {
        field: 'id',
        type: 'number',
        primary: true,
      },
    }
  })

@Controller(`${API_PREFIX}${DOCUMENT_TYPE}`)
export class DocumentTypeController implements CrudController<DocumentType> {
    constructor(public service:DocumentTypeService){};
}
