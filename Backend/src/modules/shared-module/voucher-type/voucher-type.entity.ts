import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";
import { BaseEntity } from "src/common/entities";

@Index("voucher_type_pkey", ["id"], { unique: true })
@Entity("voucher_type", { schema: "public" })
export class VoucherType extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id: number;

  @Column("character varying", { name: "vname", length: 255 })
  vname: string;

  @Column("character varying", {
    name: "description",
    nullable: true,
    length: 255
  })
  description: string | null;

  @Column("timestamp without time zone", {
    name: "createdat",
    default: () => "CURRENT_TIMESTAMP"
  })
  createdat: Date;

  @Column("timestamp without time zone", {
    name: "updatedat",
    default: () => "CURRENT_TIMESTAMP"
  })
  updatedat: Date;
}