import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { VoucherType } from './voucher-type.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class VoucherTypeService extends TypeOrmCrudService<VoucherType> {
    constructor(@InjectRepository(VoucherType) repo){
        super(repo)
    }
}
