import { Test, TestingModule } from '@nestjs/testing';
import { VoucherTypeController } from './voucher-type.controller';

describe('VoucherType Controller', () => {
  let controller: VoucherTypeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [VoucherTypeController],
    }).compile();

    controller = module.get<VoucherTypeController>(VoucherTypeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
