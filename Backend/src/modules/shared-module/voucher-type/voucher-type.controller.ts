import { Controller } from '@nestjs/common';
import {
    Crud,
    CrudController
  } from '@nestjsx/crud';
  import { API_PREFIX,CLIENTE_PREFIX, VOUCHER_TYPE } from 'src/common/constants/routes';
  import { VoucherType } from './voucher-type.entity';
  import { VoucherTypeService } from './voucher-type.service';
  @Crud({
      model: {
        type: VoucherType,
      },
      params: {
        id: {
          field: 'id',
          type: 'number',
          primary: true,
        },
      }
    })
  
@Controller(`${API_PREFIX}${VOUCHER_TYPE}`)
export class VoucherTypeController implements CrudController<VoucherType>{
    constructor(public service:VoucherTypeService){};
}
