import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VoucherType } from './voucher-type/voucher-type.entity';
import { DocumentType } from './document-type/document-type.entity'
import { VoucherTypeService } from './voucher-type/voucher-type.service';
import { VoucherTypeController } from './voucher-type/voucher-type.controller';
import { DocumentTypeService } from './document-type/document-type.service';
import { DocumentTypeController } from './document-type/document-type.controller';
@Module({
  imports: [
    TypeOrmModule.forFeature([
      VoucherType,
      DocumentType
    ])
  ],
  exports:[
    VoucherTypeService,
    DocumentTypeService
  ],
  providers:[
    VoucherTypeService,
    DocumentTypeService
  ],
  controllers:[
    VoucherTypeController,
    DocumentTypeController
  ]
})
export class SharedModuleModule {}
