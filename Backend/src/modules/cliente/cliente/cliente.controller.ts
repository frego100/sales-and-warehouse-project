import { Controller } from '@nestjs/common';
//import { ApiUseTags } from '@nestjs/swagger';
import {
  Crud,
  CrudController
} from '@nestjsx/crud';
import { API_PREFIX,CLIENTE_PREFIX } from 'src/common/constants/routes';
import { Cliente } from './cliente.entity';
import { ClienteService } from './cliente.service';
@Crud({
    model: {
      type: Cliente,
    },
    params: {
      id: {
        field: 'id',
        type: 'number',
        primary: true,
      },
    }
  })

//@ApiUseTags('cliente')
@Controller(`${API_PREFIX}${CLIENTE_PREFIX}clientes`)
export class ClienteController implements CrudController<Cliente>{
    constructor(public service: ClienteService){};
  
}
