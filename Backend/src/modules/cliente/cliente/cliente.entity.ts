import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";
import { BaseEntity } from "src/common/entities";

@Index("cliente_pkey", ["id"], { unique: true })
@Entity("cliente", { schema: "public" })
export class Cliente extends BaseEntity{
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id: number;

  @Column("character varying", { name: "nombre", length: 20 })
  nombre: string;

  @Column("character varying", { name: "apellido", length: 25 })
  apellido: string;

  @Column("integer", { name: "telefono" })
  telefono: number;

  @Column("character varying", { name: "nrcalle", length: 25 })
  nrcalle: string;

  @Column("character varying", { name: "correo", length: 25 })
  correo: string;

  @Column("timestamp without time zone", {
    name: "createdat",
    default: () => "CURRENT_TIMESTAMP",
  })
  createdat: Date;

  @Column("timestamp without time zone", {
    name: "updatedat",
    default: () => "CURRENT_TIMESTAMP",
  })
  updatedat: Date;
}