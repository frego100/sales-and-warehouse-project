import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Cliente } from './cliente.entity';

@Injectable()
export class ClienteService extends TypeOrmCrudService<Cliente>{
    constructor(@InjectRepository(Cliente)repo){
        super(repo);
    }
}
