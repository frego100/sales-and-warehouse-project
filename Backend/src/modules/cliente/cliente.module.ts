import { Module } from '@nestjs/common';
import { ClienteController } from './cliente/cliente.controller';
import { ClienteService } from './cliente/cliente.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cliente } from './cliente/cliente.entity';
@Module({
  imports:[
    TypeOrmModule.forFeature([
      Cliente
    ])
  ],
  exports:[
    ClienteService
  ],
  providers: [
    ClienteService
  ],
  controllers: [
    ClienteController
  ]
})
export class ClienteModule {}
