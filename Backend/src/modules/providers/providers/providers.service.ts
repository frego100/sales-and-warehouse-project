import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Providers } from './provider.entity';

@Injectable()
export class ProvidersService extends TypeOrmCrudService<Providers>{
    constructor(@InjectRepository(Providers)repo){
        super(repo);
    }
}
