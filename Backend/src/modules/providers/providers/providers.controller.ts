import { Controller } from '@nestjs/common';
//import { ApiUseTags } from '@nestjs/swagger';
import { Crud, CrudController, Override, ParsedRequest, CrudRequest } from '@nestjsx/crud';
import { API_PREFIX, CLIENTE_PREFIX, PROVIDER_PREFIX } from 'src/common/constants/routes';
import { Providers } from './provider.entity';
import { ProvidersService } from './providers.service';
@Crud({
  model: {
    type: Providers,
  },
  params: {
    id: {
      field: 'id',
      type: 'number',
      primary: true,
    },
  },
  query: {
    join: {
      documentType: {
        eager: true,
        required: false,
        exclude: ['createdat', 'updatedat'],
      },
    },
  },
  
})
@Controller(`${API_PREFIX}${PROVIDER_PREFIX}provider`)
export class ProvidersController implements CrudController<Providers>{
    constructor(public service:ProvidersService){};
    get base():CrudController<Providers>{
        return this;
    }
    @Override('getManyBase')
    getAll(@ParsedRequest() req:CrudRequest){
        return this.base.getManyBase(req);
    }
}
