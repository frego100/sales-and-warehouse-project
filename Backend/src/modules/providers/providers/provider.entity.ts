import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { DocumentType } from './../../shared-module/document-type/document-type.entity';
import { BaseEntity } from 'src/common/entities';

@Index('providers_document_number_key', ['documentNumber'], { unique: true })
@Index('providers_email_key', ['email'], { unique: true })
@Index('providers_pkey', ['id'], { unique: true })
@Index('providers_phone_number_key', ['phoneNumber'], { unique: true })
@Index('providers_ruc_key', ['ruc'], { unique: true })
@Entity('providers', { schema: 'public' })
export class Providers extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @Column('character varying', { name: 'pname', length: 200 })
  pname: string;

  @Column('character', { name: 'document_number', unique: true, length: 25 })
  documentNumber: string;

  @Column('character', { name: 'ruc', unique: true, length: 11 })
  ruc: string;

  @Column('character varying', { name: 'email', unique: true, length: 200 })
  email: string;

  @Column('character varying', { name: 'address', nullable: true, length: 255 })
  address: string | null;

  @Column('character varying', {
    name: 'phone_number',
    unique: true,
    length: 25,
  })
  phoneNumber: string;

  @Column('character varying', { name: 'city', nullable: true, length: 25 })
  city: string | null;

  @Column('timestamp without time zone', {
    name: 'createdat',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdat: Date;

  @Column('timestamp without time zone', {
    name: 'updatedat',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedat: Date;

  @ManyToOne(
    () => DocumentType,
    documentType => documentType.providers,
  )
  @JoinColumn([{ name: 'document_type_id', referencedColumnName: 'id' }])
  documentType: DocumentType;
}
