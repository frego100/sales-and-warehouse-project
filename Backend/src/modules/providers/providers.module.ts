import { Module } from '@nestjs/common';
import { ProvidersService } from './providers/providers.service';
import { ProvidersController } from './providers/providers.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Providers } from './providers/provider.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Providers])],
  exports:[
    ProvidersService
  ],
  providers: [ProvidersService],
  controllers: [ProvidersController],
})
export class ProvidersModule {}
