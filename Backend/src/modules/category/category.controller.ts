import { Controller } from '@nestjs/common';
import {
    Crud,
    CrudController
  } from '@nestjsx/crud';
import { API_PREFIX, CATEGORY_PREFIX } from 'src/common/constants/routes';
import { Category } from './category.entity';
import { CategoryService } from './category.service';
@Crud({
    model: {
      type: Category,
    },
    params: {
      id: {
        field: 'id',
        type: 'number',
        primary: true,
      },
    }
  })
@Controller(`${API_PREFIX}${CATEGORY_PREFIX}`)
export class CategoryController implements CrudController<Category>{
    constructor(public service: CategoryService){};
}
