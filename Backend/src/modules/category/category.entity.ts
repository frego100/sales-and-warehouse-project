import { Column, Entity, Index,  OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { BaseEntity } from "src/common/entities";
import { Products } from "./../products/products/product.entity";
@Index("category_pkey", ["id"], { unique: true })
@Entity("category", { schema: "public" })
export class Category extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id: number;

  @Column("character varying", { name: "cname", length: 255 })
  cname: string;

  @Column("character varying", {
    name: "description",
    nullable: true,
    length: 255
  })
  description: string | null;

  @Column("timestamp without time zone", {
    name: "createdat",
    default: () => "CURRENT_TIMESTAMP"
  })
  createdat: Date;

  @Column("timestamp without time zone", {
    name: "updatedat",
    default: () => "CURRENT_TIMESTAMP"
  })
  updatedat: Date;

  @OneToMany(
    () => Products,
    products => products.category
  )
  products: Products[];
}