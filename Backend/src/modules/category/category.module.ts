import { Module } from '@nestjs/common';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from './category.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Category
    ])
  ],
  exports:[
    CategoryService
  ],
  providers: [
    CategoryService
  ],
  controllers: [
    CategoryController
  ]
})
export class CategoryModule {}
