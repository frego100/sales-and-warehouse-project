import { Module } from '@nestjs/common';
import { EmpleadoController } from './empleado/empleado.controller';
import { EmpleadoService } from './empleado/empleado.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Empleado } from './empleado/empleado.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Empleado
    ])
  ],
  exports:[
    EmpleadoService
  ],
  controllers: [
    EmpleadoController
  ],
  providers: [
    EmpleadoService
  ]
})
export class EmpleadoModule {}
