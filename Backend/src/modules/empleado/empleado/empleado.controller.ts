import { Controller } from '@nestjs/common';
//import { ApiUseTags } from '@nestjs/swagger';
import {
  Crud,
  CrudController,
  Override,
  ParsedRequest,
  CrudRequest
} from '@nestjsx/crud';
import { API_PREFIX,EMPLEADO_PREFIX } from 'src/common/constants/routes';
import { Empleado } from './empleado.entity';
import { EmpleadoService } from './empleado.service';
@Crud({
    model: {
      type: Empleado,
    },
    params: {
      id: {
        field: 'id',
        type: 'number',
        primary: true,
      },
    },
    query:{
      join:{
        idTrabajo:{
          eager:true,
          required:false,
          exclude:['createdat','updatedat']
        }
      }
    }
  })
  @Controller(`${API_PREFIX}${EMPLEADO_PREFIX}empleado`)
  export class EmpleadoController implements CrudController<Empleado> {
      constructor(public service:EmpleadoService){};
      get base():CrudController<Empleado>{
        return this;
      }
      @Override('getManyBase')
      getAll(@ParsedRequest() req:CrudRequest){
        return this.base.getManyBase(req);
      }
  }
