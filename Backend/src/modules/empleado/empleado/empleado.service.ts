import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Empleado } from './empleado.entity';

@Injectable()
export class EmpleadoService extends TypeOrmCrudService<Empleado>{
    constructor(@InjectRepository(Empleado)repo){
        super(repo)
    }
}
