import {
    Column,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
  } from "typeorm";
  import { Trabajo } from "./../../trabajo/trabajo/trabajo.entity";
import { BaseEntity } from "src/common/entities";
@Entity("empleado", { schema: "public" })
@Index("personal_dni_key", ["dni"], { unique: true })
@Index("empleado_pkey", ["id"], { unique: true })
export class Empleado extends BaseEntity{
  @PrimaryGeneratedColumn({ type: "integer", name: "id" })
  id: number;

  @Column("character varying", { name: "nombres", length: 200 })
  nombres: string;

  @Column("character varying", { name: "apellido_materno", length: 200 })
  apellidoMaterno: string;

  @Column("character varying", { name: "apellido_paterno", length: 200 })
  apellidoPaterno: string;

  @Column("character", { name: "dni", unique: true, length: 8 })
  dni: string;

  @Column("boolean", { name: "estado", default: () => "true" })
  estado: boolean;

  @Column("character varying", { name: "correo", length: 200 })
  correo: string;

  @Column("character varying", { name: "direccion", length: 255 })
  direccion: string;

  @Column("character varying", { name: "num_telefono", length: 25 })
  numTelefono: string;

  @Column("date", { name: "fecha_nacimiento", nullable: true })
  fechaNacimiento: string | null;

  @Column("date", { name: "fecha_ingreso", nullable: true })
  fechaIngreso: string | null;

  @Column("timestamp without time zone", {
    name: "createdat",
    default: () => "CURRENT_TIMESTAMP",
  })
  createdat: Date;

  @Column("timestamp without time zone", {
    name: "updatedat",
    default: () => "CURRENT_TIMESTAMP",
  })
  updatedat: Date;

  @ManyToOne(() => Trabajo, (trabajo) => trabajo.empleados)
  @JoinColumn([{ name: "id_trabajo", referencedColumnName: "id" }])
  idTrabajo: Trabajo;
}