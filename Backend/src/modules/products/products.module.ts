import { Module } from '@nestjs/common';
import { ProductsService } from './products/products.service';
import { ProductsController } from './products/products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Products } from './products/product.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Products
    ])
  ],
  exports:[
    ProductsService
  ],
  providers: [
    ProductsService
  ],
  controllers: [
    ProductsController
  ]
})
export class ProductsModule {}
