import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Category } from './../../category/category.entity';
import { BaseEntity } from 'src/common/entities';

@Index('products_pkey', ['id'], { unique: true })
@Entity('products', { schema: 'public' })
export class Products extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id: number;

  @Column('character varying', { name: 'pname', length: 200 })
  pname: string;

  @Column('character varying', { name: 'model', length: 50 })
  model: string;

  @Column('character varying', {
    name: 'description',
    nullable: true,
    length: 255,
  })
  desciption: string | null;

  @Column('numeric', { name: 'price' })
  price: string;

  @Column('numeric', { name: 'stock' })
  stock: string;

  @Column('timestamp without time zone', {
    name: 'createdat',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdat: Date;

  @Column('timestamp without time zone', {
    name: 'updatedat',
    default: () => 'CURRENT_TIMESTAMP',
  })
  updatedat: Date;

  @ManyToOne(
    () => Category,
    category => category.products,
  )
  @JoinColumn([{ name: 'category_id', referencedColumnName: 'id' }])
  category: Category;
}
