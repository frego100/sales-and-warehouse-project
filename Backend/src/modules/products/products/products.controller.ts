import { Controller } from '@nestjs/common';
//import { ApiUseTags } from '@nestjs/swagger';
import { Crud, CrudController, Override, ParsedRequest, CrudRequest } from '@nestjsx/crud';
import { API_PREFIX, PRODUCT_PREFIX } from 'src/common/constants/routes';
import { Products } from './product.entity';
import { ProductsService } from './products.service';
@Crud({
  model: {
    type: Products,
  },
  params: {
    id: {
      field: 'id',
      type: 'number',
      primary: true,
    },
  },
  query: {
    join: {
      category: {
        eager: true,
        required: false,
        exclude: ['createdat', 'updatedat'],
      },
    },
  },
})
@Controller(`${API_PREFIX}${PRODUCT_PREFIX}product`)
export class ProductsController implements CrudController<Products> {
  constructor(public service: ProductsService) {}
  get base(): CrudController<Products> {
    return this;
  }
  @Override('getManyBase')
  getAll(@ParsedRequest() req: CrudRequest) {
    return this.base.getManyBase(req);
  }
}
